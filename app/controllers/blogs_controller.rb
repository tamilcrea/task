class BlogsController < ApplicationController

    before_action :set_blog, only: [:show, :edit, :update, :destroy]

    def index
        @blogs = Blog.all.paginate(page: params[:page], per_page: 5)
    end

    def new
        @blog = Blog.new
       @categories = Category.all.map { |c| [c.name, c.id]}
    end

    def create
        @blog = Blog.new(blog_params)
        if @blog.save
            flash[:info]="Blog created sucessfully"
           redirect_to blogs_path  
        else
         flash[:warning] ='invalid title && content && Category'
         redirect_to 'new_blog_path'
        end
    end

    def show
      
    end
    
    def edit
         @categories = Category.all.map { |c| [c.name, c.id]}
    end
    def update
        if @blog.update(blog_params)
         redirect_to blogs_path
        else
          render 'edit' 
        end 
    end

    def destroy
        @blog.destroy
        redirect_to blogs_path
    end

    private
    def blog_params
        params.require(:blog).permit(:title, :content, :author, :category_id)
    end

    def set_blog
        @blog = Blog.find(params[:id])
    end
end
