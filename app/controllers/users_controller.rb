class UsersController < ApplicationController

def destroy
	@user = User.find(session[:guest_user_id])
	session.delete(:guest_user_id)
	redirect_to   new_user_session_path
end 
end
