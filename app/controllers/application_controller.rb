class ApplicationController < ActionController::Base


	before_action :authenticate_user!


	protect_from_forgery

    add_flash_types :danger, :info, :success, :warning

	def current_or_guest_user
    if current_user
      if session[:guest_user_id]  != current_user.id
        logging_in

        guest_user(with_retry = false).try(:reload).try(:destroy)
        session[:guest_user_id] = nil
      end
      current_user
    else
      guest_user
    end
  end

def current_user
  super || guest_user
end

def guest_user(with_retry = true)
 @cached_guest_user = User.find( session[:guest_user_id] ||= create_guest_user)
  rescue ActiveRecord::RecordNotFound #
     session[:guest_user_id] = nil
     guest_user if with_retry
end


private
def create_guest_user
  @user = User.create(:password => "guest", :email => "guest_#{Time.now.to_i}#{rand(99)}@gmail.com", guest: true)
  @user.save(:validate => false)
  session[:guest_user_id]= @user.id
end
end
